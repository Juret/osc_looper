#include "osclooper/loop.h"
#include <log4cxx/basicconfigurator.h>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr loop::logger = log4cxx::Logger::getLogger("osclooper.loop");

loop::loop()
{
  if(logger->getAllAppenders().size() == 0)
  {
    log4cxx::BasicConfigurator::configure();
  }
}

loop::~loop() {}

std::string loop::get_name()
{
  LOG4CXX_DEBUG(logger, "message");
  return std::string("Not implemented yet");
}
