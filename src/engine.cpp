#include "osclooper/engine.h"

#include <thread>
#include <sstream>
#include <stdexcept>
#include <iomanip>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr engine::logger = log4cxx::Logger::getLogger("osclooper.engine");
std::mutex engine::latch;
std::condition_variable engine::condition;

engine::engine() :
  engine("noName")
{
}

engine::engine(std::string name) :
  engine(name, 1s)
{
}

engine::engine(std::string name,
               std::chrono::microseconds period,
               unsigned short int osc_port) :
  _name(name),
  _period(period),
  _running_thread(false),
  _osc_server(osc_port),
  _osc_port(osc_port)
{
  if(logger->getAllAppenders().size() == 0)
  {
    log4cxx::BasicConfigurator::configure();
  }

  engineManager *eMgr;
  eMgr = engineManager::instance();

  if(eMgr->get_engines()->count(name) != 0)
  {
    std::ostringstream output;
    output << "Engine name "
           << std::quoted(name)
           << " is already taken. NOT constructed!";
    LOG4CXX_ERROR(logger, output.str());
    throw std::invalid_argument(output.str());
  }

  _osc_base_path << "/" << _name;
  osc_initialization();
  _osc_server.start();
  start();
}

engine::~engine()
{
  std::ostringstream output;
  stop();
  output << std::quoted(get_name())
         << " engine has been destructed";
  LOG4CXX_DEBUG(logger, output.str());
}

std::string engine::get_name()
{
  return _name;
}

std::chrono::microseconds engine::get_period()
{
  return _period;
}

loopManager *engine::get_loop_manager()
{
  return loopMgr;
}

unsigned short int engine::get_osc_port()
{
  return _osc_port;
}

bool engine::is_running()
{
  std::lock_guard<std::mutex> locker(latch);
  return _running_thread;
}

void engine::osc_initialization()
{
  for(auto command : control_commands)
  {
    if(_osc_server.add_method(_osc_base_path.str() + command.first,
                              command.second,
                              control_handler,
                              this))
    {
      std::ostringstream output;
      output << "OSC method " << std::quoted(_osc_base_path.str() + command.first);

      if(!command.second.empty())
      {
        output << " " << std::quoted(command.second);
      }

      output << " has been registered ";
      LOG4CXX_INFO(logger, output.str());
    }
  }

  _osc_server.start();
  LOG4CXX_INFO(logger, std::string("OSC server URL: ") + _osc_server.url());
}

void engine::start()
{
  std::ostringstream output;
  _running_thread = true;

  if(_thread.joinable() == false)
  {
    _thread = std::thread(&engine::run, this);
    output << std::quoted(get_name())
           << " engine started";
  }
  else
  {
    output << std::quoted(get_name())
           << " engine is ALREADY started";
  }

  condition.notify_one();
  LOG4CXX_DEBUG(logger, output.str());
}

void engine::stop()
{
  {
    std::lock_guard<std::mutex> locker(latch);
    _running_thread = false;
  }
  condition.notify_all();
  std::ostringstream output;

  if(_thread.joinable())
  {
    _thread.join();
    output << std::quoted(get_name())
           << " engine stopped";
  }
  else
  {
    output << std::quoted(get_name())
           << " engine ALREADY stopped";
  }

  LOG4CXX_DEBUG(logger, output.str());
}

void engine::run()
{
  // TODO:
  //   * listen to incoming messages
  //   * analyse message for looping compliance (be sure that the message is not a future one, for example)
  //   * write message in the static loop vector (recorder)
  // TODO:
  //   * set the target OSC address to the config one
  //   * sequencely read the atomic loop vector
  //   * send each message to the target (player)
  using clock = std::chrono::high_resolution_clock;
  std::unique_lock<std::mutex> locker(latch);

  while(_running_thread)
  {
    std::ostringstream future_date, thread_id, output;
    clock::time_point future = clock::now() + _period;
    execute_cycle();
    future_date << std::chrono::duration_cast<std::chrono::microseconds>(future - clock::now()).count();
    thread_id << _thread.get_id();
    output << std::quoted(get_name())
           << " engine, thread #"
           << thread_id.str()
           << " tack, next in "
           << future_date.str();
    LOG4CXX_TRACE(logger, output.str());
    condition.wait_until(locker, future);
  }
}

void engine::execute_cycle()
{
  std::ostringstream output;
  output << "Engine name: "
         << std::quoted(get_name());
  LOG4CXX_TRACE(logger, output.str());
}

int engine::control_handler(const char *path,
                            const char *types,
                            lo_arg **arguments,
                            int nbarguments,
                            void *,//message,
                            void *user_data)
{
  engine *my_engine = static_cast<engine *>(user_data);
  std::string command(std::string(path).substr(std::string(path).rfind("/")));
  std::string arguments_types(types);
  std::ostringstream output;

  // We have to discriminate (sometimes) within the arguments number
  // and types
  if(command == "/ping")
  {
    unsigned int nbloops = 0;
    std::string return_url(&arguments[0]->s);
    validate_returl(return_url);
    std::string return_path(&arguments[1]->s);
    lo::Message return_message("ssi",
                               my_engine->_osc_server.url().c_str(),
                               VERSION,
                               nbloops);
    lo::Address dest(return_url.c_str());
    dest.send(return_path.c_str(),
              return_message);
  }
  else if(command == "/start")
  {
    if(nbarguments == 0
       || (nbarguments == 1
           && arguments_types == "T")
      )
    {
      my_engine->start();
    }
    else if(nbarguments == 1)
    {
      my_engine->stop();
    }
  }
  else if(command == "/stop")
  {
    if(nbarguments == 0
       || (nbarguments == 1
           && arguments_types == "T")
      )
    {
      my_engine->stop();
    }
    else if(nbarguments == 1)
    {
      my_engine->start();
    }
  }
  else if(command == "/new_loop")
  {
    if(nbarguments == 2)
    {
      // add new loop in the loop_manager with quantize to the lowest
    }
    else if(nbarguments == 3)
    {
      // add loop in the loop_manager with specified quantize
    }
  }
  else if(command == "/remove_loop")
  {
    // remove loop from loop_manager by name
  }

  output << my_engine->get_name()
         << " - " << command
         << " " << types
         << " " << nbarguments
         << " @ " << my_engine->_osc_server.url();
  // << arguments[0]->d;
  LOG4CXX_INFO(logger, output.str());
  return 0;
}

void engine::validate_returl(std::string &returl)
{
  if(returl.substr(0, 10) != "osc.udp://")
  {
    returl = "osc.udp://" + returl;
  }
}
