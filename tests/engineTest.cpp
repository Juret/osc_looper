#include "engineTest.h"
#include "osclooper/engine.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(engineTest);

void engineTest::setUp()
{
  log4cxx::BasicConfigurator::configure();
}

void engineTest::tearDown() {}

void engineTest::testDefaultConstructor()
{
  engine test_engine;
  CPPUNIT_ASSERT("noName" == test_engine.get_name());
  CPPUNIT_ASSERT(1s == test_engine.get_period());
}

void engineTest::testNameConstructor()
{
  std::string _name = "testNameConstructor";
  engine test_engine(_name);
  CPPUNIT_ASSERT(_name == test_engine.get_name());
  CPPUNIT_ASSERT(1s == test_engine.get_period());
}

void engineTest::testNamePeriodConstructor()
{
  std::string _name = "testNamePeriodConstructor";
  std::chrono::microseconds _period = 100ms;
  engine test_engine(_name, _period);
  CPPUNIT_ASSERT(_name == test_engine.get_name());
  CPPUNIT_ASSERT(_period == test_engine.get_period());
}

void engineTest::testNamePeriodOSCPortConstructor()
{
  std::string _name = "testNamePeriodOSCPortConstructor";
  std::chrono::microseconds _period = 100ms;
  unsigned int _osc_port = 9001;
  engine test_engine(_name, _period, _osc_port);
  CPPUNIT_ASSERT(_name == test_engine.get_name());
  CPPUNIT_ASSERT(_period == test_engine.get_period());
  CPPUNIT_ASSERT(_osc_port == test_engine.get_osc_port());
}

void engineTest::testRunningThread()
{
  engine test_engine;
  CPPUNIT_ASSERT(test_engine.is_running());
}

void engineTest::testRestartingThread()
{
  engine test_engine;
  test_engine.stop();
  test_engine.start();
  CPPUNIT_ASSERT(test_engine.is_running());
}

void engineTest::testStopThread()
{
  engine test_engine;
  test_engine.stop();
  CPPUNIT_ASSERT(!test_engine.is_running());
}
