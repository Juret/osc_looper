#ifndef ENGINETEST_H
#define ENGINETEST_H

#include <cppunit/extensions/HelperMacros.h>

class engineTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(engineTest);
  CPPUNIT_TEST(testDefaultConstructor);
  CPPUNIT_TEST(testNameConstructor);
  CPPUNIT_TEST(testNamePeriodConstructor);
  CPPUNIT_TEST(testNamePeriodOSCPortConstructor);
  CPPUNIT_TEST(testRunningThread);
  CPPUNIT_TEST(testRestartingThread);
  CPPUNIT_TEST(testStopThread);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testDefaultConstructor();
  void testNameConstructor();
  void testNamePeriodConstructor();
  void testNamePeriodOSCPortConstructor();
  void testRunningThread();
  void testRestartingThread();
  void testStopThread();
};

#endif // ENGINETEST_H
