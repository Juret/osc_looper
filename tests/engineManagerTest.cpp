#include "engineManagerTest.h"
#include "osclooper/engineManager.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(engineManagerTest);

void engineManagerTest::setUp() {}

void engineManagerTest::tearDown() {}

void engineManagerTest::testSingleton()
{
  engineManager *test_engineManager, *test_other_engineManager;
  test_engineManager = engineManager::instance();
  test_other_engineManager = engineManager::instance();
  CPPUNIT_ASSERT(test_engineManager == test_other_engineManager);
}

void engineManagerTest::testAddEngine()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  test_engineManager->add("testAddEngine", 10ms);
  // CPPUNIT_ASSERT(condition)
}

// void engineTest::testConflictNameConstructor()
// {
//   std::string _name = "testConflictNameConstructor";
//   engine test_engine(_name, 1ms, 9000);
//   CPPUNIT_ASSERT_THROW(engine test_engine_name_already_used(_name, 1ms, 9001), std::invalid_argument);
// }

// void engineManagerTest::testConflictConstructor()
// {
//   std::string _name = "testConflictConstructor";
//   engine test_engine(_name);
//   CPPUNIT_ASSERT_THROW(engine test_engine_name_already_used(_name), std::invalid_argument);
// }

// void engineManagerTest::testConflictOSCPortAddEngine()
// {
//   std::string _name = "testConflictOSCPortAddEngine";
//   std::string _other_name = "testConflictOSCPortAddEngine_otherName";
//   engine test_engine(_name);
//   CPPUNIT_ASSERT_THROW(engine test_engine_osc_port_already_used(_other_name), std::invalid_argument);
// }

// void engineTest::testEngineRegistrySize()
// {
//   std::vector<engine *> test_engines;

//   for(unsigned int i = 0; i < MAX_ENGINES; ++i)
//   {
//     std::ostringstream compteur;
//     compteur << i;
//     test_engines.push_back(new engine(compteur.str(), 1s));
//     test_engines.back()->stop();
//   }

//   test_engines.shrink_to_fit();
//   CPPUNIT_ASSERT(MAX_ENGINES == test_engines[0]->engines.size());

//   for(auto engine : test_engines)
//   {
//     delete engine;
//   }
// }

// void engineManagerTest::testRegistryMaxSize()
// {
//   std::vector<engine *> test_engines;

//   for(unsigned int i = 0; i < MAX_ENGINES; ++i)
//   {
//     std::ostringstream compteur;
//     compteur << i;
//     test_engines.push_back(new engine(compteur.str(), 1s));
//     test_engines.back()->stop();
//   }

//   test_engines.shrink_to_fit();
//   CPPUNIT_ASSERT_THROW(test_engines.push_back(new engine("one_more_is_too_much", 1s)), std::length_error);

//   for(auto engine : test_engines)
//   {
//     delete engine;
//   }
// }
