#ifndef ENGINEMANAGERTEST_H
#define ENGINEMANAGERTEST_H

#include <cppunit/extensions/HelperMacros.h>

class engineManagerTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(engineManagerTest);
  CPPUNIT_TEST(testSingleton);
  CPPUNIT_TEST(testAddEngine);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testSingleton();
  void testAddEngine();
};

#endif // ENGINEMANAGERTEST_H
