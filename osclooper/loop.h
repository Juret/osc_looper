#ifndef LOOP_H
#define LOOP_H

#include <chrono>
#include <thread>
#include <cstdint>

#include <log4cxx/logger.h>

class loop
{
 public:
  explicit loop();
  ~loop();

  std::string get_name();

  static log4cxx::LoggerPtr logger;

 private:

};

#endif // LOOP_H
